# BTTB12 Tutorial Corryvreckan

**This repo contains the configuration and geomety files for the Corryvreckan hands-on course given at the [BTTB12](https://indico.cern.ch/event/1323113/).**

## Which Corryvreckan Version is needed?
The examples presented in this tutorial work out-of-the box with [**Corryvreckan v2.0.1**](https://gitlab.cern.ch/corryvreckan/corryvreckan/-/tree/v2.0.1).

For the tutorial, you can download a **virtual machine image** with a ready-to-use installation. A guideline and some trouble-shooting can be found [**here**](./SetupVirtualMachine.md).
If you prefer a local installation on your laptop:

  * See installation instructions on the [**web page**](https://project-corryvreckan.web.cern.ch/project-corryvreckan/page/installation/) or download the [**manual**](https://gitlab.cern.ch/corryvreckan/corryvreckan/-/jobs/artifacts/master/raw/public/usermanual/corryvreckan-manual.pdf?job=cmp%3Ausermanual).
  * Examples 1 and 2 can be tested with the default compilation of Corryvreckan, i.e. no flags need to be changed and no additional packages are required. Please note: if you hit `Ctrl+C`, i.e. stop the analysis, too early, you won't get many tracks because we need to wait for the SPS spill in the data. This only starts at about 10sec.
  * Example 3 illustrates how to use the modules `EventDefinitionM26` and `EventLoaderEUDAQ2`. To compile these, you need a local EUDAQ2 installation (as described in the manual) and enable the build of both modules in cmake.
  * To download the data files execute the scripts in [**here**](./data).

## For future reference:
All runs are reduced to about 10% of their full size.

__CLICpix2 at SPS__:
* Run 29243 with alignment file updated after upgrading to Corryvreckan v2.0.

__ATLASpix at SPS__:
* Run 29674 with alignment file updated after upgrading to Corryvreckan v2.0.

__DESY__:
* ATLASpix:
  - Run 715 with alignment file updated after upgrading to Corryvreckan v2.0.
* CLICTD:
  - Run 3390 with alignment file updated after upgrading to Corryvreckan v2.0 and clictd/geometry/mask_CLICTD_run3390.txt from Magdalena's fork of the testbeam analysis repo.
