[Timepix3_0]
number_of_pixels = 256, 256
orientation = 9.91532deg,187.426deg,-1.35837deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 943.029um,285.818um,0
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_1]
number_of_pixels = 256, 256
orientation = 10.2871deg,187.568deg,-1.08404deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -231.714um,405.912um,21.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_2]
number_of_pixels = 256, 256
orientation = 9.68648deg,187.952deg,-1.65121deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 63.889um,386.384um,43.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_3]
number_of_pixels = 256, 256
orientation = 8.99876deg,8.99412deg,-0.0178763deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -10.202um,-11.943um,186.5mm
role = "reference"
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_4]
number_of_pixels = 256, 256
orientation = 7.61501deg,9.72407deg,1.16883deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 468.341um,-579.756um,231.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_5]
number_of_pixels = 256, 256
orientation = 6.6965deg,10.2627deg,-0.392992deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -898.955um,28.374um,336.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

